import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';

import Item from './models/Item';

const app = express();
const router = express.Router();

app.use(cors());
app.use(bodyParser.json());

//mongoose.connect('mongodb://localhost:27017/Hackathon');
mongoose.connect('mongodb://heroku_1fwl6jx4:kimkl81n0bli3eb5l5d0ea3u9g@ds145911.mlab.com:45911/heroku_1fwl6jx4');

const connection = mongoose.connection;

connection.once('open', () => {
    console.log('MongoDB database connection established successfully!');
});

router.route('/items').post((req, res) => {
    let item = new Item(req.body);
    item.save()
        .then(item => {
            res.status(200).json({'item': 'Added successfully'});
        })
        .catch(err => {
            res.status(400).send('Failed to create new record');
        });
});

router.route('/items').get((req, res) => {
    Item.find((err, items) => {
        if (err)
            return500Error(res, err);
        else if (!items.length)
            res.status(404).json('No records found');
        else
            res.json(items);
    });
});

router.route('/items/:id').get((req, res) => {
    Item.findById(req.params.id, (err, item) => {
        if (err)
            return500Error(res, err);
        else if (!item)
            return404Error(res, req.params.id);
        else
            res.json(item);
    })
});

router.route('/items/:id').put((req, res) => {
    Item.findById(req.params.id, (err, item) => {
        if (err)
            return500Error(res, err);
        else if (!item)
            return404Error(res, req.params.id);
        else {
            item.name = req.body.name;
            item.category = req.body.category;
            item.providesDiscount = req.body.providesDiscount;
            item.isInsurable = req.body.isInsurable;
            item.sentToAgent = req.body.sentToAgent;
            item.location = req.body.location;
            item.description = req.body.description;
            item.imageURL = req.body.imageURL;

            item.save().then(item => {
                res.json('Update done');
            }).catch(err => {
                res.status(400).send('Update failed');
            });
        }
    });
});

router.route('/items/:id').delete((req, res) => {
    Item.findByIdAndRemove({_id: req.params.id}, (err, item) => {
        if (err)
            return500Error(res, err);
        else if (!item)
            return404Error(res, req.params.id);
        else
            res.json('Item removed successfully');
    });
});

function return500Error(res, error) {
    console.log(error);
    res.status(500).json('It done broke');
}

function return404Error(res, id) {
    res.status(404).json('No record found for ' + id);
}

app.use('/', router);

//app.listen(4000, () => console.log(`Express server running on port 4000`));
app.listen((process.env.PORT || 4000), () => console.log(`Express server running on port ` + (process.env.PORT || `4000`)));