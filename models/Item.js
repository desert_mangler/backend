import mongoose from 'mongoose';

const Schema = mongoose.Schema;


let Location = new Schema({
    longitude: {
        type: String
    },
    latitude: {
        type: String
    },
    room: {
        type: String
    }
})

let Item = new Schema({
    name: {
        type: String
    },
    category: {
        type: String
    },
    providesDiscount: {
        type: Boolean
    },
    isInsurable: {
        type: Boolean
    },
    sentToAgent: {
        type: Boolean
    },
    value: {
        type: Number
    },
    location: {
        type: Location
    },
    description: {
        type: String
    },
    imageURL: {
        type: String
    }
});

export default mongoose.model('Item', Item);